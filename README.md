# test-just-digital

> Just Digital Quiz

## Run

``` bash
	This software are meant to be served over an HTTP server.
	Opening index.html over file:// won't work.

	The aplication runs on /dist path.
	So, the url should be something like http://localhost/test-just-digital/dist
```
